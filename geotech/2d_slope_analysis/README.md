# Dynamic effective stress analysis of slope
These are 2D analyses. These analyses will make use of material parameters given in the OpenSees example of effective stress analysis of 2D slope.  
1. ex1: Effective stress analysis of a simple slope with the same foundation material as the slope.
